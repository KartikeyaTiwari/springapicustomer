package in.raghav.customerproject.service;

import java.util.List;

import in.raghav.customerproject.entity.PurchasedItem;
import in.raghav.customerproject.entity.EntityHelper.SumOfPrice;


public interface PurchasedItemService {

	void insertSingleItem(PurchasedItem singleITem);

	void insertMultiple(List<PurchasedItem> itemdto);

	SumOfPrice getGrandTotaldetails(List<PurchasedItem> itemdto, SumOfPrice total);

	PurchasedItem getSingleItem(int itemid);

	List<PurchasedItem> getMultipleItem(Integer[] idlist);
}
