package in.raghav.customerproject.service.ServiceImpl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.raghav.customerproject.entity.PurchasedItem;
import in.raghav.customerproject.entity.EntityHelper.SumOfPrice;
import in.raghav.customerproject.repository.HomeShoppingRepo;
import in.raghav.customerproject.repository.PurchasedItemRepo;
import in.raghav.customerproject.service.PurchasedItemService;

@Service
public class PurchasedItemImpl implements PurchasedItemService {

    @Autowired
    PurchasedItemRepo purchaseItemrepo;

    @Autowired
    HomeShoppingRepo homeshoprepo;
    Long a_itemPrice = 0L;
    Long b_itemPrice = 0L;

    List<PurchasedItem> items = new ArrayList<>();
    PurchasedItem itemlist;

    @Override
    public void insertSingleItem(PurchasedItem singleITem) {
        purchaseItemrepo.save(singleITem);
    }

    @Override
    public void insertMultiple(List<PurchasedItem> itemdto) {
        for (int index = 0; index < itemdto.size(); index++) {
            System.out.println(itemdto.get(index));
            a_itemPrice = itemdto.get(index).getItemPrice();
            b_itemPrice = a_itemPrice + b_itemPrice;
            System.out.println("\n \n price  :" + a_itemPrice);
            purchaseItemrepo.save(itemdto.get(index));
        }
        System.out.println("\n\n Grand total :" + b_itemPrice);
        b_itemPrice = 0l;
    }

    @Override
    public SumOfPrice getGrandTotaldetails(List<PurchasedItem> itemdto, SumOfPrice total) {
        for (int index = 0; index < itemdto.size(); index++) {
            System.out.println(itemdto.get(index));
            a_itemPrice = itemdto.get(index).getItemPrice();
            b_itemPrice = a_itemPrice + b_itemPrice;
            System.out.println("price  :" + a_itemPrice);
        }
        System.out.println("\n Grand total :" + b_itemPrice);
        total.setTotalsum(b_itemPrice);
        b_itemPrice = 0l;
        return total;
    }

    @Override
    public PurchasedItem getSingleItem(int itemid) {
        return purchaseItemrepo.findById(itemid).get();
    }

    @Override
    public List<PurchasedItem> getMultipleItem(Integer[] idlist) {
        items.clear();
        for (int id = 0; id < idlist.length; id++) {
            System.out.println("its here :: " + idlist[id]);
            itemlist = purchaseItemrepo.findById(idlist[id]).get();
            items.add(itemlist);
        }
        return items;
    }

}
