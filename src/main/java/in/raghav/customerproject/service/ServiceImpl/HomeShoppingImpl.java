package in.raghav.customerproject.service.ServiceImpl;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.raghav.customerproject.entity.Customer;
import in.raghav.customerproject.entity.HomeShopping;
import in.raghav.customerproject.entity.PurchasedItem;
import in.raghav.customerproject.entity.EntityHelper.UserShopingData;
import in.raghav.customerproject.repository.CustomerRepo;
import in.raghav.customerproject.repository.HomeShoppingRepo;
import in.raghav.customerproject.repository.PurchasedItemRepo;
import in.raghav.customerproject.service.HomeShoppService;

@Service
public class HomeShoppingImpl implements HomeShoppService {

  @Autowired
  HomeShoppingRepo shoppingrepo;

  @Autowired
  CustomerRepo customerRepo;

  @Autowired
  PurchasedItemRepo purchaseItemrepo;

  PurchasedItem purchasedItem;
  List<PurchasedItem> items = new ArrayList<>();

  Long a_itemPrice = 0L;
  Long b_itemPrice = 0L;

  private int itemid;

  @Override
  public void addHomeShoppingForCustomer(HomeShopping userShopData, int id) {
    Customer cust = customerRepo.findById(id).get();
    userShopData.setCustommer(cust);
    shoppingrepo.save(userShopData);
    customerRepo.save(cust);
  }

  @Override
  public List<HomeShopping> getcartdetail(int uid) {
    List<HomeShopping> homeshop;
    Customer cust = customerRepo.findById(uid).get();
    homeshop = cust.getHomeShopping();
    return homeshop;
  }

  @Override
  public void deleteCartItem(int hscid) {
    HomeShopping homeshop = shoppingrepo.findById(hscid).get();
    shoppingrepo.delete(homeshop);
  }

  @Override
  public UserShopingData getAllShopData(List<PurchasedItem> purchasedItems, UserShopingData usd) {
    for (int index = 0; index < purchasedItems.size(); index++) {
      System.out.println(purchasedItems.get(index));
      a_itemPrice = purchasedItems.get(index).getItemPrice();
      b_itemPrice = a_itemPrice + b_itemPrice;
      System.out.println("price  :" + a_itemPrice);
    }
    System.out.println("\n Grand total :" + b_itemPrice);
    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
    LocalDateTime now = LocalDateTime.now();
    System.out.println(dtf.format(now));

    usd.setDate(now);
    usd.setPurchasedItems(purchasedItems);
    usd.setSumOfPrice(b_itemPrice);

    b_itemPrice = 0L;
    return usd;
  }

  @Override
  public HomeShopping addCartToUser(int userid, List<PurchasedItem> itembuy, HomeShopping home) {
    Customer cust = customerRepo.findById(userid).get();
    home.setCustommer(cust);

    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
    LocalDateTime now = LocalDateTime.now();
    System.out.println(dtf.format(now));
    home.setDate(now);

    for (int index = 0; index < itembuy.size(); index++) {
      itemid = itembuy.get(index).getItemId();
      purchasedItem = purchaseItemrepo.findById(itemid).get();

      System.out.println(itembuy.get(index));
      a_itemPrice = itembuy.get(index).getItemPrice();
      b_itemPrice = a_itemPrice + b_itemPrice;
      System.out.println("price  :" + a_itemPrice);
      items.add(purchasedItem);
      home.setPurchaseItem(items);
    }

    home.setTotalbuy(b_itemPrice);
    shoppingrepo.save(home);
    customerRepo.save(cust);

    b_itemPrice = 0l;
    return home;
  }

}