package in.raghav.customerproject.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.raghav.customerproject.entity.Customer;
import in.raghav.customerproject.repository.CustomerRepo;

@Service
public class CustomerService {

        @Autowired
        private CustomerRepo customerRepo;

        public List<Customer> getAllCutomer() {
                List<Customer> customers = new ArrayList<>();
                customerRepo.findAll().forEach(customers::add);
                return customers;
        }

        public Customer addSingleCustomerData(Customer customerdata) {
                return customerRepo.save(customerdata);
        }

        public Optional<Customer> getSingleCustomerById(Integer cid) {
                return customerRepo.findById(cid);
        }

        public void delByCustomerId(Integer cid) {
                customerRepo.deleteById(cid);
        }

        public void updateCustomerMail(Integer cid, String newMail) {
                Customer cst = customerRepo.findById(cid).get();
                cst.setEmail(newMail);
                customerRepo.save(cst);
        }

        public Customer getSingleCustomerByEmailId(String Email) {
                return customerRepo.findByEmail(Email);
        }

        public List<Customer> getByFirstLastName(String firstname, String lastname) {
                return customerRepo.findByFirst_NameAndLast_Name(firstname, lastname);
        }

        public void updateCustomerContact(Integer cid, Long contactinfo) {
                Customer cst = customerRepo.findById(cid).get();
                cst.setContact(contactinfo);
                customerRepo.save(cst);
        }

}

// iretrable vs list