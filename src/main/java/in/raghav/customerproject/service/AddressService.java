package in.raghav.customerproject.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.raghav.customerproject.entity.Address;
import in.raghav.customerproject.entity.Customer;
import in.raghav.customerproject.repository.AddressRepo;
import in.raghav.customerproject.repository.CustomerRepo;

@Service
public class AddressService {

  @Autowired
  private AddressRepo addressrepo;

  @Autowired
  private CustomerRepo customerrepoo;

  public Address getSingleBlock(int id) {
    return addressrepo.findByAddressid(id);
  }

  // public List<Customer> getCustomerDetail_And_Address(int id) {
  // return customerrepoo.findByCustomerAddressAddressid(id);
  // }

  public Customer addSingleCustomerAddressTRY(Customer cust, Address address) {
    cust.setCustomerAddress(address);
    customerrepoo.save(cust);
    addressrepo.save(address);
    return cust;
  }

  public void updateCustomerAddress(int cid, Address address) {
    Customer cust = customerrepoo.findById(cid).get();
    Address adr = cust.getCustomerAddress();
    adr = address;
    addressrepo.save(adr);

  }

  public void addCustomerAddressIfmissing(Address address, int cid) {
    Customer cust = customerrepoo.findById(cid).get();
    cust.setCustomerAddress(address);
    customerrepoo.save(cust);
    addressrepo.save(address);

  }

}
