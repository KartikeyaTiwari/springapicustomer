package in.raghav.customerproject.service;

import java.util.List;

import in.raghav.customerproject.entity.HomeShopping;
import in.raghav.customerproject.entity.PurchasedItem;
import in.raghav.customerproject.entity.EntityHelper.UserShopingData;

public interface HomeShoppService {

	void addHomeShoppingForCustomer(HomeShopping userShopData, int id);

	List<HomeShopping> getcartdetail(int uid);

	void deleteCartItem(int hscid);

	UserShopingData getAllShopData(List<PurchasedItem> purchasedItems, UserShopingData usd);

	HomeShopping addCartToUser(int userid, List<PurchasedItem> itembuy,HomeShopping home);

}
