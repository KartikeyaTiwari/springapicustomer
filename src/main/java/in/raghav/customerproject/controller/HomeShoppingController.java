package in.raghav.customerproject.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import in.raghav.customerproject.entity.HomeShopping;
import in.raghav.customerproject.entity.PurchasedItem;
import in.raghav.customerproject.entity.EntityHelper.UserShopingData;
import in.raghav.customerproject.service.HomeShoppService;



@RestController
@RequestMapping("/api/homeshop")
public class HomeShoppingController {

    @Autowired
    HomeShoppService homeshopservice;

    @PostMapping("/shoping/{id}") // send details of single customer
    public void addHomeShopping(@RequestBody HomeShopping userShopData, @PathVariable("id") int uid) {
        homeshopservice.addHomeShoppingForCustomer(userShopData, uid );
    }

    @GetMapping("/getcart/{id}")
    public List<HomeShopping> getUserShopingCart(@PathVariable("id") int uid) {
        return homeshopservice.getcartdetail(uid);
    }

    @DeleteMapping("/cart/{id}")
    public void deleteCartItem(@PathVariable("id") int hscid) {
        homeshopservice.deleteCartItem(hscid);
    }

    @RequestMapping("/shopdata")
    public UserShopingData getShopData(@RequestBody List<PurchasedItem> purchasedItems,UserShopingData usd){
        homeshopservice.getAllShopData(purchasedItems,usd);
        return usd;
    }

   @PostMapping("/usercart/{userid}")
   public HomeShopping addCartToUser(@PathVariable("userid") int userid, @RequestBody List<PurchasedItem> itembuy,
           HomeShopping home) {
        System.out.println("\n user id: "+userid);
        System.out.println("\n item by list: "+itembuy);
        homeshopservice.addCartToUser(userid,itembuy,home);
        return home;
   }


}
