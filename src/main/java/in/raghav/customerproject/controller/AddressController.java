package in.raghav.customerproject.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import in.raghav.customerproject.entity.Address;
import in.raghav.customerproject.entity.Customer;
import in.raghav.customerproject.entity.EntityHelper.UserSimpleData;
import in.raghav.customerproject.service.AddressService;

@RestController
@RequestMapping("/apis")
public class AddressController {

    @Autowired
    private AddressService addressService;

    @RequestMapping("/address/{id}") 
    public Address getSingleCustomerAddress(@PathVariable("id") Integer cid) {
        return addressService.getSingleBlock(cid);
    }
    
    @PostMapping("/customer/addresstry") // send details of single customer, customer and address
    public Customer addCustomerAddresstry(@RequestBody UserSimpleData usersimpledata) {
        return addressService.addSingleCustomerAddressTRY(usersimpledata.getCustomer(), usersimpledata.getAddress());
    }

    @PutMapping("/customer/addaddress/{id}") // send details of single customer if missing //no need to pass address id
    public void addCustomerAddressIfmissing(@RequestBody Address address, @PathVariable("id") int cid) {
        addressService.addCustomerAddressIfmissing(address, cid);
    }

    @PutMapping("/getcustomeraddress/{id}") // update address of customer by id need to pass address id
    public void UpdateCustomerContact(@PathVariable("id") int cid, @Validated @RequestBody Address address) {
        addressService.updateCustomerAddress(cid, address);
    }

}
