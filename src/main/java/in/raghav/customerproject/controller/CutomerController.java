package in.raghav.customerproject.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import in.raghav.customerproject.entity.Customer;
import in.raghav.customerproject.service.CustomerService;

@RestController
@RequestMapping("/api")
public class CutomerController {

    @Autowired
    private CustomerService customerService;

    @RequestMapping("/customers") 
    public List<Customer> getAllCustomers() {
        return customerService.getAllCutomer();
    }

    @PostMapping("/customer") 
    public Customer addCustomer(@RequestBody Customer customerdata) {
        return customerService.addSingleCustomerData(customerdata);
    }

    @RequestMapping("/customer/{id}") 
    public Optional<Customer> getCustomerById(@PathVariable("id") Integer cid) {
        return customerService.getSingleCustomerById(cid);
    }

    @DeleteMapping("/customer/{id}")
    public void delByCustomerId(@PathVariable("id") Integer cid) {
        customerService.delByCustomerId(cid);
    }

    @PutMapping("/customer/{id}/{newemail:.+}") 
    public void UpdateCustomer(@PathVariable("id") Integer cid, @PathVariable("newemail") String newEmail) {
        customerService.updateCustomerMail(cid, newEmail);
    }

    @RequestMapping("/customer1/{newemail:.+}")
    public Customer getCustomerByEmail(@PathVariable("newemail") String newEmail) {
        return customerService.getSingleCustomerByEmailId(newEmail);
    }

    @RequestMapping("/customer2/{fistname}/{lastname}") 
    public List<Customer> getByNameTag(@PathVariable("fistname") String firstname,
            @PathVariable("lastname") String lastname) {
        return customerService.getByFirstLastName(firstname, lastname);
    }

    @PutMapping("/getconcustomer/{id}/{contactinfo}") 
    public void UpdateCustomerContact(@PathVariable("id") int cid, @PathVariable("contactinfo") Long contactinfo) {
        customerService.updateCustomerContact(cid, contactinfo);
    }

    @RequestMapping("/cc")
    public String gettt() {
        return "hi";
    }

}

