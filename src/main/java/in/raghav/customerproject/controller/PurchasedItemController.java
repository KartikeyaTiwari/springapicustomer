package in.raghav.customerproject.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import in.raghav.customerproject.entity.PurchasedItem;
import in.raghav.customerproject.entity.EntityHelper.SumOfPrice;
import in.raghav.customerproject.service.PurchasedItemService;
import org.springframework.web.bind.annotation.RequestMethod;

@RestController
@RequestMapping("/api/cart")
public class PurchasedItemController {

    @Autowired
    PurchasedItemService purchasedItemService;

    @PostMapping("/insert")
    public void insertItem(@RequestBody PurchasedItem singleITem) {
        purchasedItemService.insertSingleItem(singleITem);
    }

    @PostMapping("/multiple1")
    public void insertMultipleItem(@RequestBody List<PurchasedItem> itemdto) {
        System.out.println("in controller :" + itemdto);
        purchasedItemService.insertMultiple(itemdto);
    }

    @RequestMapping("/getAmounts")
    public SumOfPrice getGrandTotalofITem(@RequestBody List<PurchasedItem> itemdto, SumOfPrice total) {
        purchasedItemService.getGrandTotaldetails(itemdto, total);
        return total;
    }

    @GetMapping("/item/{itemid}")
    public PurchasedItem getSingleItem(@PathVariable("itemid") int itemid) {
        return purchasedItemService.getSingleItem(itemid);
    }

    @RequestMapping(value = "/itemlist", method = RequestMethod.GET)
    public List<PurchasedItem> requestMethodName(@RequestBody Integer[] idlist) {
        return purchasedItemService.getMultipleItem(idlist);
    }

}
