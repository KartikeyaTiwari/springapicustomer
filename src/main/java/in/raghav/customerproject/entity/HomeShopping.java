package in.raghav.customerproject.entity;

import java.time.LocalDateTime;
//import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "homeShopping")
public class HomeShopping {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "hscid")
    private int hscid;

    @Column(name = "date")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime date;

    @ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH, CascadeType.REFRESH })
    @JoinTable(name = "Home_shop_Item", 
    joinColumns = @JoinColumn(name = "hscid_id"), 
    inverseJoinColumns = @JoinColumn(name = "itemid_id"))
    private List<PurchasedItem> purchaseItem;

    @Column(name = "totalbuy")
    private long totalbuy;

    @JsonIgnore
    @ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH, CascadeType.REFRESH })
    private Customer custommer;

    public HomeShopping() {

    }

    public HomeShopping(int hscid, LocalDateTime date, List<PurchasedItem> purchaseItem, long totalbuy,
            Customer custommer) {
        this.hscid = hscid;
        this.date = date;
        this.purchaseItem = purchaseItem;
        this.totalbuy = totalbuy;
        this.custommer = custommer;
    }

    public int getHscid() {
        return this.hscid;
    }

    public void setHscid(int hscid) {
        this.hscid = hscid;
    }

    public LocalDateTime getDate() {
        return this.date;
    }

    public void setDate(LocalDateTime now) {
        this.date = now;
    }

    public long getTotalbuy() {
        return this.totalbuy;
    }

    public void setTotalbuy(long totalbuy) {
        this.totalbuy = totalbuy;
    }

    public Customer getCustommer() {
        return this.custommer;
    }

    public void setCustommer(Customer custommer) {
        this.custommer = custommer;
    }

    public List<PurchasedItem> getPurchaseItem() {
        return this.purchaseItem;
    }

    public void setPurchaseItem(List<PurchasedItem> purchaseItem) {
        this.purchaseItem = purchaseItem;
    }

    @Override
    public String toString() {
        return "{" + " hscid='" + getHscid() + "'" + ", date='" + getDate() + "'" + ", purchaseItem='"
                + getPurchaseItem() + "'" + ", totalbuy='" + getTotalbuy() + "'" + ", custommer='" + getCustommer()
                + "'" + "}";
    }

}
