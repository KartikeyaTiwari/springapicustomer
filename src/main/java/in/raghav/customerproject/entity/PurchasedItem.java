package in.raghav.customerproject.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "purchase_item")
public class PurchasedItem {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "item_id")
    private int itemId;

    @Column(name = "item_name")
    private String itemName;

    @Column(name = "item_price")
    private Long itemPrice;

    @Column(name = "item_type")
    private String itemType;

    @JsonIgnore
    @ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH, CascadeType.REFRESH })
    @JoinTable(name = "Home_shop_Item", joinColumns = @JoinColumn(name = "itemid_id"), inverseJoinColumns = @JoinColumn(name = "hscid_id"))
    private List<HomeShopping> homeShopping;

    public PurchasedItem() {
    }

    public int getItemId() {
        return this.itemId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    public String getItemName() {
        return this.itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public Long getItemPrice() {
        return this.itemPrice;
    }

    public void setItemPrice(Long itemPrice) {
        this.itemPrice = itemPrice;
    }

    public String getItemType() {
        return this.itemType;
    }

    public void setItemType(String itemType) {
        this.itemType = itemType;
    }

    public List<HomeShopping> getHomeShopping() {
        return this.homeShopping;
    }

    public void setHomeShopping(List<HomeShopping> homeShopping) {
        this.homeShopping = homeShopping;
    }

    @Override
    public String toString() {
        return "{" + " itemId='" + getItemId() + "'" + ", itemName='" + getItemName() + "'" + ", itemPrice='"
                + getItemPrice() + "'" + ", itemType='" + getItemType() + "'" + ", homeShopping='" + getHomeShopping()
                + "'" + "}";
    }

}