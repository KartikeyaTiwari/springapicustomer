package in.raghav.customerproject.entity.EntityHelper;

import java.time.LocalDateTime;
import java.util.List;

import in.raghav.customerproject.entity.PurchasedItem;

public class UserShopingData {

    private LocalDateTime date;
    private List<PurchasedItem> purchasedItems;
    private Long sumOfPrice;

    public UserShopingData() {
    }

    public UserShopingData(LocalDateTime date, List<PurchasedItem> purchasedItems, Long sumOfPrice) {
        this.date = date;
        this.purchasedItems = purchasedItems;
        this.sumOfPrice = sumOfPrice;
    }

    public LocalDateTime getDate() {
        return this.date;
    }

    public void setDate(LocalDateTime now) {
        this.date = now;
    }

    public List<PurchasedItem> getPurchasedItems() {
        return this.purchasedItems;
    }

    public void setPurchasedItems(List<PurchasedItem> purchasedItems) {
        this.purchasedItems = purchasedItems;
    }

    public Long getSumOfPrice() {
        return this.sumOfPrice;
    }

    public void setSumOfPrice(Long b_itemPrice) {
        this.sumOfPrice = b_itemPrice;
    }

    @Override
    public String toString() {
        return "{" + " date='" + getDate() + "'" + ", purchasedItems='" + getPurchasedItems() + "'" + ", sumOfPrice='"
                + getSumOfPrice() + "'" + "}";
    }

}
