package in.raghav.customerproject.entity.EntityHelper;

public class SumOfPrice {

    private Long totalsum;

    public Long getTotalsum() {
        return this.totalsum;
    }

    public void setTotalsum(Long totalsum) {
        this.totalsum = totalsum;
    }

    public SumOfPrice(Long totalsum) {
        this.totalsum = totalsum;
    }

    public SumOfPrice() {
    }

    @Override
    public String toString() {
        return "{" + " totalsum='" + getTotalsum() + "'" + "}";
    }

}
