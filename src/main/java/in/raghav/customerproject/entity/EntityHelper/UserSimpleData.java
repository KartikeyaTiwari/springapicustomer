package in.raghav.customerproject.entity.EntityHelper;

import in.raghav.customerproject.entity.Address;
import in.raghav.customerproject.entity.Customer;

public class UserSimpleData {
    private Customer customer;
    private Address address;

    public Customer getCustomer() {
        return this.customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Address getAddress() {
        return this.address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

}
