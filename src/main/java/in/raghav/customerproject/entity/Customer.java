package in.raghav.customerproject.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

//import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "customer")
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "first_name")
    private String first_name;

    @Column(name = "last_name")
    private String last_name;

    @Column(name = "email")
    private String email;

    @Column(name = "contact")
    private Long contact;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "customer_address_id", referencedColumnName = "addressid")
    private Address customerAddress;

    @OneToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH, CascadeType.REFRESH,
            CascadeType.REMOVE }, mappedBy = "custommer")
    private List<HomeShopping> homeShopping;

    public Customer() {

    }

    public Customer(int id, String first_name, String last_name, String email, Long contact, Address customerAddress,
            List<HomeShopping> homeShopping) {
        this.id = id;
        this.first_name = first_name;
        this.last_name = last_name;
        this.email = email;
        this.contact = contact;
        this.customerAddress = customerAddress;
        this.homeShopping = homeShopping;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirst_name() {
        return this.first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return this.last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getContact() {
        return this.contact;
    }

    public void setContact(Long contact) {
        this.contact = contact;
    }

    public Address getCustomerAddress() {
        return this.customerAddress;
    }

    public void setCustomerAddress(Address customerAddress) {
        this.customerAddress = customerAddress;
    }

    public List<HomeShopping> getHomeShopping() {
        return this.homeShopping;
    }

    public void setHomeShopping(List<HomeShopping> homeShopping) {
        this.homeShopping = homeShopping;
    }

    @Override
    public String toString() {
        return "{" + " id='" + getId() + "'" + ", first_name='" + getFirst_name() + "'" + ", last_name='"
                + getLast_name() + "'" + ", email='" + getEmail() + "'" + ", contact='" + getContact() + "'"
                + ", customerAddress='" + getCustomerAddress() + "'" + ", homeShopping='" + getHomeShopping() + "'"
                + "}";
    }

}