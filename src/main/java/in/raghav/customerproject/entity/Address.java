package in.raghav.customerproject.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

//import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "address")
public class Address {

    @Id
    // @JsonIgnore
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "addressid")
    private int addressid;

    @Column(name = "district")
    private String district;

    @Column(name = "location")
    private String location;

    @Column(name = "landmark")
    private String landmark;

    @Column(name = "pincode")
    private int pincode;

    public Address() {

    }

    public Address(String district, String location, String landmark, int pincode) {
        this.district = district;
        this.location = location;
        this.landmark = landmark;
        this.pincode = pincode;
    }

    public int getAddressid() {
        return this.addressid;
    }

    public void setAddressid(int addressid) {
        this.addressid = addressid;
    }

    public String getDistrict() {
        return this.district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getLocation() {
        return this.location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public int getPincode() {
        return this.pincode;
    }

    public void setPincode(int pincode) {
        this.pincode = pincode;
    }

    public String getLandmark() {
        return this.landmark;
    }

    public void setLandmark(String landmark) {
        this.landmark = landmark;
    }

    @Override
    public String toString() {
        return "{" + " addressid='" + getAddressid() + "'" + ", district='" + getDistrict() + "'" + ", location='"
                + getLocation() + "'" + ", pincode='" + getPincode() + "'" + ", landmark='" + getLandmark() + "'" + "}";
    }

}
