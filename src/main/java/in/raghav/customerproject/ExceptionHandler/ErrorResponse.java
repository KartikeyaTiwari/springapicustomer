package in.raghav.customerproject.ExceptionHandler;

public class ErrorResponse {
    private int status;
    private String message;
    private Throwable cause;
    private long timestamp;

    public ErrorResponse() {
    }

    public ErrorResponse(int status, String message, Throwable throwable, long timestamp) {
        this.status = status;
        this.message = message;
        this.cause = throwable;
        this.timestamp = timestamp;
    }

    public int getStatus() {
        return this.status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Throwable getCause() {
        return this.cause;
    }

    public void setCause(Throwable cause) {
        this.cause = cause;
    }

    public long getTimestamp() {
        return this.timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

}
