package in.raghav.customerproject.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import in.raghav.customerproject.entity.Address;
import in.raghav.customerproject.entity.Customer;

public interface CustomerRepo extends CrudRepository<Customer, Integer> {

     Customer findByEmail(String email);

     @Query("SELECT c FROM Customer c WHERE c.first_name = ?1 and c.last_name = ?2")
     List<Customer> findByFirst_NameAndLast_Name(String firstname, String lastname);

     public List<Customer> findByCustomerAddressAddressid(int id);

     void save(Address addressinfo);
}
