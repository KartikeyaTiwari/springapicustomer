package in.raghav.customerproject.repository;

import in.raghav.customerproject.entity.HomeShopping;
import in.raghav.customerproject.entity.EntityHelper.UserShopingData;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

public interface HomeShoppingRepo extends CrudRepository<HomeShopping, Integer> {

	void save(List<HomeShopping> homeShopping);
	void save(UserShopingData usd);
}
