package in.raghav.customerproject.repository;

import org.springframework.data.repository.CrudRepository;

import in.raghav.customerproject.entity.Address;

public interface AddressRepo extends CrudRepository<Address, Integer> {
    Address findByAddressid(int id);
}
