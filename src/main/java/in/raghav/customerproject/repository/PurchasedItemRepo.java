package in.raghav.customerproject.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import in.raghav.customerproject.entity.PurchasedItem;

public interface PurchasedItemRepo extends CrudRepository<PurchasedItem, Integer> {
	void save(List<PurchasedItem> itembuy);
}